/****** Object:  Table [dbo].[users]    Script Date: 2/10/2020 9:30:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[users](
	[user_code] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[surname] [varchar](50) NOT NULL,
	[mobile] [varchar](10) NULL,
	[email] [varchar](64) NOT NULL,
	[address] [varchar](256) NOT NULL,
	[birth_date] [date] NOT NULL,
	[user_password] [varchar](128) NOT NULL,
	[is_admin] [bit] NOT NULL,
	[createdAt] [date] NOT NULL,
	[updatedAt] [date] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[user_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO


