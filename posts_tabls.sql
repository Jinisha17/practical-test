/****** Object:  Table [dbo].[posts]    Script Date: 2/10/2020 9:30:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[posts](
	[post_code] [int] IDENTITY(1,1) NOT NULL,
	[user_code] [int] NOT NULL,
	[post_title] [varchar](max) NOT NULL,
	[post_body] [varchar](max) NOT NULL,
	[createdAt] [date] NOT NULL,
	[updatedAt] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[posts]  WITH CHECK ADD FOREIGN KEY([user_code])
REFERENCES [dbo].[users] ([user_code])
GO


