const Sequelize = require('sequelize');
const db = require('../config/database')

const Post = db.define('posts', {
    post_code: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    user_code: {
        type: Sequelize.NUMBER,
        allowNull: false
    },
    post_title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    post_body: {
        type: Sequelize.STRING,
        allowNull: false
    },
    createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
}, {});


module.exports = Post