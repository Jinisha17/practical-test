let Joi = require('@hapi/joi')

module.exports = {
    registartionSchema: Joi.object().keys({
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        surname: Joi.string().required(),
        mobile: Joi.string().max(10).min(10).required(),
        email: Joi.string().email().required(),
        password: Joi.string().min(5).max(10).required(),
        address: Joi.string().min(10).required(),
        birth_date: Joi.date().required()
    }),
    loginSchema: Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().min(5).max(10).required(),
    })
}