let Joi = require('@hapi/joi')

module.exports = {
    addPostSchema: Joi.object().keys({
        post_title: Joi.string().required(),
        post_body: Joi.string().required()
    }),
    updatePostSchema: Joi.object().keys({
        post_code: Joi.number().required(),
        post_title: Joi.string().optional(),
        post_body: Joi.string().optional()
    }),
    deletePostSchema: Joi.object().keys({
        post_code: Joi.number().required()
    })
}