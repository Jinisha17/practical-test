const winston = require('winston')

const myFormat = winston.format.printf(info => {
  return `${info.timestamp} ${info.level}: ${info.message}`
})

const logger = winston.createLogger({
  transports: [
    new (winston.transports.Console)({
      level: 'debug',
      json: false,
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        myFormat
      )
    })]
})

module.exports = {
  logCons: require('./constants/log-constants'),
  urlCons: require('./constants/url-constants'),
  msgCons: require('./constants/msg-constants'),
  statusCodeCons: require('./constants/status-code-constants'),
  httpStatusCode: require('./constants/http-status-codes'),
  printLog: (level, msg) => {
    logger.log(level, msg)
  }
}
