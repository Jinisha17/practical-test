module.exports = {
  URL_POST_REGISTRATION: 'user/registration',
  URL_POST_LOGIN: 'user/login',
  URL_GET_PROFILE: 'user/profile',

  URL_POST_ADD_POST: 'post/add',
  URL_GET_FETCH_POST: 'post/fetch',
  URL_PUT_UPDATE_POST: 'post/update',
  URL_DELETE_REMOVE_POST: 'post/delete'
}
