module.exports = {
  DATA_FOUND: 'Data found',
  DATA_NOT_FOUND: 'Data not found',
  BAD_REQUEST: 'Bad Request',

  USER_EXIST: 'User is already registred',
  USER_REGISTRATION_SUCCESS: 'Use is successfuly registred',
  USER_REGISTRATION_ERROR: 'Internal server error while registration',
  
  LOGIN_SUCCESS: 'User logged in successfully',
  INVALID_LOGIN_CRED: 'Email or password is incorrect',
  LOGIN_ERROR: 'Internal server error while login',

  TOKEN_NOT_FOUND: 'Auth token is not supplied',
  TOKEN_INVALID: 'Invalid token',
  TOKEN_EXPIRED: 'Auth token is expired',

  USER_DATA_SUCCESS: 'User profile found successfuly',
  USER_DATA_SUCCESS_NO_DATA: 'No such user found',
  USER_DATA_ERROR: 'Internal server error while finding user profile',

  POST_ADD_SUCCESS: 'Post added successfuly',
  POST_ADD_ERROR:'Internal server error while adding post',

  POST_FETCH_SUCCESS: 'Post fetch successfuly',
  POST_FETCH_ERROR:'Internal server error while fetching post',

  POST_UPDATE_SUCCESS: 'Post update successfuly',
  POST_UPDATE_ERROR: 'Internal server error while updating post',

  POST_DELETED_SUCCESS: 'Post deleted successfuly',
  POST_DELETE_ERROR: 'Internal server error while deleting post',
  
  ACCESS_DENIED: 'You have not rights to access this post',

  RESPONSE_DATA: 'data',
  RESPONSE_STATUS_CODE: 'status_code',
  RESPONSE_STATUS_MSG: 'message',
  RESPONSE_ERROR_STATUS: 'error_status',
  RESPONSE_ERRORS: 'errors'
}
