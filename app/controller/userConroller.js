const express = require('express')
const router = express.Router()

// Load Function helper
const logger = require('./../utils/logger')
const func = require('../helpers/function-helper')
const schema = require('../policies/userSchema')
const userService = require('../service/userService')
const authorization = require('../service/authorizationService')


//  User regitration
const userRegistration = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - userRegistration :: userRegistration() :: API - ${logger.urlCons.URL_POST_REGISTRATION}`)
    let validation = schema.registartionSchema.validate(req.body)
    if (!validation.error) {    //  valide payload
        logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_LEVEL_DEBUG} :: CONTROLLER - userRegistration :: userRegistration() :: valide payload  :: ${req.data}`)
        userService.userRegistration(req.body, (error, response) => {
            if (error) {
                res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                res.send(error)
            } else {
                res.status(logger.httpStatusCode.OK)
                res.send(response)
            }
        })
    } else {    //   invalid payload
        logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: CONTROLLER - userRegistration :: userRegistration() :: invalide payload`)
        res.status(logger.httpStatusCode.BAD_REQUEST)
        res.send(func.responseGenerator(null, logger.statusCodeCons.BAD_REQUEST_400, logger.msgCons.BAD_REQUEST, validation.error.details[0].message))
    }
}

//  User login
const userLogin = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - userRegistration :: userLogin() :: API - ${logger.urlCons.URL_POST_LOGIN}`)
    let validation = schema.loginSchema.validate(req.body)
    if (!validation.error) {    //  valide payload
        logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_LEVEL_DEBUG} :: CONTROLLER - userRegistration :: userLogin() :: valide payload  :: ${req.data}`)
        userService.userLogin(req.body, (error, response) => {
            if (error) {
                res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                res.send(error)
            } else {
                res.status(logger.httpStatusCode.OK)
                res.send(response)
            }
        })
    } else {    //  invalid payload
        logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: CONTROLLER - userRegistration :: userLogin() :: invalide payload`)
        res.status(logger.httpStatusCode.BAD_REQUEST)
        res.send(func.responseGenerator(null, logger.statusCodeCons.BAD_REQUEST_400, logger.msgCons.BAD_REQUEST, validation.error.details[0].message))
    }

}

// Fetch user profile
const userProfileFetch = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - userRegistration :: userProfileFetch() :: API - ${logger.urlCons.URL_GET_PROFILE}`)
    const authorize = authorization.checkToken(req, res);
    if (authorize) {    //  authorized user
        userService.userProfileFetch(req.session, (error, response) => {
            if (error) {
                res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                res.send(error)
            } else {
                res.status(logger.httpStatusCode.OK)
                res.send(response)
            }
        })
    }
}
/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     }
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */
router.get('/profile', userProfileFetch)
router.post('/register', userRegistration)
router.post('/login', userLogin)

module.exports = router