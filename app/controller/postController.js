const express = require('express')
const router = express.Router()

// Load Function helper
const logger = require('./../utils/logger')
const schema = require('../policies/postSchema')
const func = require('../helpers/function-helper')
const postService = require('../service/postService')
const authorization = require('../service/authorizationService')


// Add new post
const addPost = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - postController :: addPost() :: API - ${logger.urlCons.URL_POST_ADD_POST}`)
    const authorize = authorization.checkToken(req, res);
    if (authorize) { // autorized user
        let validation = schema.addPostSchema.validate(req.body)
        if (!validation.error) {    //  valide payload
            postService.addPost(req, (error, response) => {
                if (error) {
                    res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                    res.send(error)
                } else {
                    res.status(logger.httpStatusCode.OK)
                    res.send(response)
                }
            })
        } else {    //  invalid payload
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: CONTROLLER - postController :: addPost() :: invalide payload`)
            res.status(logger.httpStatusCode.BAD_REQUEST)
            res.send(func.responseGenerator(null, logger.statusCodeCons.BAD_REQUEST_400, logger.msgCons.BAD_REQUEST, validation.error.details[0].message))
        }
    }

}

//   Fetch user posts
const fetchPost = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - postController :: fetchPost() :: API - ${logger.urlCons.URL_GET_FETCH_POST}`)
    const authorize = authorization.checkToken(req, res);
    if (authorize) { // authorized user
        postService.fetchPost(req, (error, response) => {
            if (error) {
                res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                res.send(error)
            } else {
                res.status(logger.httpStatusCode.OK)
                res.send(response)
            }
        })
    }
}

//  Update user post
const updatePost = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - postController :: updatePost() :: API - ${logger.urlCons.URL_PUT_UPDATE_POST}`)
    const authorize = authorization.checkToken(req, res);
    if (authorize) {    //  authorized user
        let validateObj = {
            post_code: req.params['post_code']
        }
        let validation = schema.updatePostSchema.validate({
            ...validateObj,
            ...req.body
        })
        if (!validation.error) { // valide payload
            postService.updatePost(req, (error, response) => {
                if (error) {
                    res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                    res.send(error)
                } else {
                    res.status(logger.httpStatusCode.OK)
                    res.send(response)
                }
            })
        } else { // invalid payload
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: CONTROLLER - updatePost :: addPost() :: invalide payload`)
            res.status(logger.httpStatusCode.BAD_REQUEST)
            res.send(func.responseGenerator(null, logger.statusCodeCons.BAD_REQUEST_400, logger.msgCons.BAD_REQUEST, validation.error.details[0].message))
        }
    }
}

//  Delete user post
const deletePost = function (req, res) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: CONTROLLER - postController :: deletePost() :: API - ${logger.urlCons.URL_DELETE_REMOVE_POST}`)
    const authorize = authorization.checkToken(req, res);
    if (authorize) {    //  authorized user
        let validation = schema.deletePostSchema.validate({
            post_code: req.params['post_code']
        })
        if (!validation.error) {    //  valide user payload
            postService.deletePost(req, (error, response) => {
                if (error) {
                    res.status(logger.httpStatusCode.INTERNAL_SERVER_ERROR)
                    res.send(error)
                } else {
                    res.status(logger.httpStatusCode.OK)
                    res.send(response)
                }
            })
        } else {    //  invalid user payload
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: CONTROLLER - updatePost :: deletePost() :: invalide payload`)
            res.status(logger.httpStatusCode.BAD_REQUEST)
            res.send(func.responseGenerator(null, logger.statusCodeCons.BAD_REQUEST_400, logger.msgCons.BAD_REQUEST, validation.error.details[0].message))
        }
    }
}

router.post('/add', addPost);
router.get('/fetch', fetchPost)
router.put('/update/:post_code', updatePost)
router.delete('/delete/:post_code', deletePost)

module.exports = router