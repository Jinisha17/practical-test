'use strict'

// Load loggertion
const logger = require('../utils/logger')

exports.responseGenerator = (responseData, responseStatusCode, responseStatusMsg, responseErrors) => {
  let responseJson = {}
  // data
  if (responseErrors === null) {
    responseJson[logger.msgCons.RESPONSE_ERROR_STATUS] = false
  } else {
    responseJson[logger.msgCons.RESPONSE_ERROR_STATUS] = true
  }
  responseJson[logger.msgCons.RESPONSE_STATUS_CODE] = responseStatusCode
  responseJson[logger.msgCons.RESPONSE_STATUS_MSG] = responseStatusMsg
  responseJson[logger.msgCons.RESPONSE_ERRORS] = responseErrors
  responseJson[logger.msgCons.RESPONSE_DATA] = responseData
  
  return responseJson
}
