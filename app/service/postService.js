const func = require('../helpers/function-helper')
const logger = require('../utils/logger')
const Post = require('../models/Post')
const User = require('../models/User')

Post.belongsTo(User, { foreignKey: 'user_code' })

//  add post
const addPost = (reqData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - postService :: addPost()`)
    let postData = {
        user_code: reqData.session.user_code,
        post_title: reqData.body.post_title,
        post_body: reqData.body.post_body
    }
    Post.create(postData)
        .then(post => { //  succes
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: addPost() :: with successfuly add post`)
            return callback(null, func.responseGenerator(null, logger.statusCodeCons.POST_ADD_SUCCESS_200, logger.msgCons.POST_ADD_SUCCESS, null))
        })
        .catch(error => {   // error
            logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: addPost() :: error while insert :: ${error}`)
            return callback(func.responseGenerator(null, logger.statusCodeCons.POST_ADD_ERROR_500, logger.msgCons.POST_ADD_ERROR, error))
        });
}

//  Fetch post
const fetchPost = (reqData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - postService :: fetchPost()`)
    Post.findAll({
        include: [{model: User}]
    })
        .then( post => {
            if (post.length) {  // success
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: fetchPost() :: with successfuly fetch posts`)
                return callback(null, func.responseGenerator(getFormattedResponse(JSON.stringify(post)), logger.statusCodeCons.POST_FETCH_SUCCESS_200, logger.msgCons.POST_FETCH_SUCCESS, null))
            } else {    // no data foud
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: fetchPost() :: with successfuly fetch posts`)
                return callback(null, func.responseGenerator(null, logger.statusCodeCons.DATA_NOT_FOUND_200, logger.msgCons.DATA_NOT_FOUND, null))
            }
        })
        .catch(error => {   //  error
            logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: fetchPost() :: error while fetch posts :: ${error}`)
            return callback(func.responseGenerator(null, logger.statusCodeCons.POST_FETCH_ERROR_500, logger.msgCons.POST_FETCH_ERROR, error))
        });
}

//  Update post
const updatePost = (reqData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - postService :: updatePost()`)
    let updateCondition = {
        post_code: reqData.params['post_code']
    }
    if (!reqData.session.is_admin)  //  Add user_code condition only when user loged in
        updateCondition['user_code'] = reqData.session.user_code
    Post.update(reqData.body, {
            where: updateCondition
        })
        .then(post => {
            if (post[0]) {  // success
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: updatePost() :: with successfuly update post`)
                return callback(null, func.responseGenerator(null, logger.statusCodeCons.POST_UPDATE_SUCCESS_200, logger.msgCons.POST_UPDATE_SUCCESS, null))
            } else {    //  access denied
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: updatePost() :: with no such access`)
                return callback(null, func.responseGenerator(null, logger.statusCodeCons.ACCESS_DENIED_403, logger.msgCons.ACCESS_DENIED, null))
            }
        })
        .catch(error => {   //  error
            logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: updatePost() :: error while updating :: ${error}`)
            return callback(func.responseGenerator(null, logger.statusCodeCons.POST_UPDATE_ERROR_500, logger.msgCons.POST_UPDATE_ERROR, error))
        })
}

//  Delete post
const deletePost = (reqData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - postService :: deletePost()`)
    let deleteCondition = {
        post_code: reqData.params['post_code']
    }
    if (!reqData.session.is_admin) // Add user_code condition only when user loged in
        deleteCondition['user_code'] = reqData.session.user_code
    Post.destroy({
            where: deleteCondition
        }).then(post => {
            if (post) { //  success
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: deletePost() :: with successfuly delete posts`)
                return callback(null, func.responseGenerator(post, logger.statusCodeCons.POST_DELETE_SUCCESS_200, logger.msgCons.POST_DELETED_SUCCESS, null))
            } else {    //  access denied
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE -  postService :: deletePost() :: with no such access`)
                return callback(null, func.responseGenerator(null, logger.statusCodeCons.ACCESS_DENIED_403, logger.msgCons.ACCESS_DENIED, null))
            }
        })
        .catch(error => {   //  error
            logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: deletePost() :: error while deleting posts :: ${error}`)
            return callback(func.responseGenerator(null, logger.statusCodeCons.POST_DELETE_ERROR_500, logger.msgCons.POST_DELETE_ERROR, error))
        });
}

function getFormattedResponse(data) {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - postService :: getFormattedResponse()`)
    logger.printLog(logger.logCons.LOG_LEVEL_DEBUG, `SERVICE - postService :: getFormattedResponse() :: data - ${data}`)
    data = JSON.parse(data)
    data.forEach(element => {
        delete element.user_code
        element['user_name'] = `${element.user.first_name} ${element.user.surname}`
        delete element.user
    });
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - postService :: getFormattedResponse()`)
    return data
}
module.exports = {
    addPost: addPost,
    fetchPost: fetchPost,
    updatePost: updatePost,
    deletePost: deletePost
}
