const jwt = require('jsonwebtoken')
const logger = require('./../utils/logger')
const func = require('../helpers/function-helper')
const config = require('../config/configuration')

const checkToken = (req, res) => {
	logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC}: checkToken()`)
	let tokenFlag = false;

	let token = req.headers['x-access-token'] || req.headers['token'];
	logger.printLog(logger.logCons.LOG_LEVEL_DEBUG, `${logger.logCons.LOG_PARAM}:Token - ${token}`)
	if (!token) {	//	token not provided
		const response = func.responseGenerator(null, logger.statusCodeCons.TOKEN_NOT_FOUND_401, logger.msgCons.TOKEN_NOT_FOUND, null)
		res.status(logger.httpStatusCode.UNAUTHORIZED)
		res.send(response)
		return tokenFlag
	}

	if (token) {
		jwt.verify(token, config.secret_key, (err, decoded) => {
			if (err) {	//	token error
				let errMessage = '';
				logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `Token Error:: ${err}`)
				switch (err.name) {
					case 'TokenExpiredError':
						errMessage = logger.msgCons.TOKEN_EXPIRED
						break;
					case 'JsonWebTokenError':
						errMessage = err.message
						break;
					default:
						errMessage = err.message
						break;
				}
				const response = func.responseGenerator(null, logger.statusCodeCons.TOKEN_INVALID_401, errMessage, null)
				res.status(logger.httpStatusCode.UNAUTHORIZED)
				res.send(response)
			} else {	// success
				req.session = decoded;
				logger.printLog(logger.logCons.LOG_LEVEL_DEBUG, `${logger.logCons.LOG_PARAM}: Token decoded - ${JSON.stringify(req.session)}`)
				tokenFlag = true;
			}
		})
	} else {	//	invalide token
		token = null;
		const response = func.responseGenerator(null, logger.statusCodeCons.TOKEN_INVALID_401, logger.msgCons.TOKEN_INVALID, null)
		res.status(logger.httpStatusCode.UNAUTHORIZED)
		res.send(response)
	}
	logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC}: checkToken()`)
	return tokenFlag
};

module.exports = {
	checkToken: checkToken
}
