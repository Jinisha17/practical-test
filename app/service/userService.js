const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const func = require('../helpers/function-helper')
const logger = require('../utils/logger')
const User = require('../models/User')
const config = require('../config/configuration')

// Registration
const userRegistration = (reqData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - userRegistration :: userRegistration()`)
    const userData = {
        first_name: reqData.first_name,
        last_name: reqData.last_name,
        surname: reqData.surname,
        mobile: reqData.mobile,
        email: reqData.email,
        user_password: reqData.password,
        is_admin: reqData.is_admin ? reqData.is_admin : 0,
        address: reqData.address,
        birth_date: reqData.birth_date
    }

    User.findOne({
            where: {
                email: reqData.email
            }
        }).then(user => {
            if (!user) {    // No such user exist
                const hash = bcrypt.hashSync(userData.user_password, 10)
                userData.user_password = hash
                User.create(userData)
                    .then(user => {     //  success
                        logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userRegistration() :: with successfuly registration`)
                        return callback(null, func.responseGenerator(null, logger.statusCodeCons.USER_REGISTRATION_SUCCESS_200, logger.msgCons.USER_REGISTRATION_SUCCESS, null))
                    })
                    .catch(error => {   //error
                        logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userRegistration() :: error while insert :: ${error}`)
                        return callback(func.responseGenerator(null, logger.statusCodeCons.USER_REGISTRATION_ERROR_500, logger.msgCons.USER_REGISTRATION_ERROR, error))
                    });
            } else {    // user already exist
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userRegistration() :: with existing user`)
                return callback(null, func.responseGenerator(null, logger.statusCodeCons.USER_ALREADY_EXIST_200, logger.msgCons.USER_EXIST, false))
            }
        })
        .catch(error => {   //  error
            logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userRegistration() :: error while checking user entry :: ${error}`)
            return callback(func.responseGenerator(null, logger.statusCodeCons.USER_REGISTRATION_ERROR_500, logger.msgCons.USER_REGISTRATION_ERROR, error))
        })
}

//  Login
const userLogin = (reqData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - userRegistration :: userLogin()`)
    User.findOne({
        where: {
            email: reqData.email
        }
    }).then(user => {
        if(user){   //  user exist
            if(bcrypt.compareSync(reqData.password, user.user_password)){   //  valide credentials
                let sessionData = {
                    user_code: user.dataValues.user_code,
                    first_name: user.dataValues.first_name,
                    last_name: user.dataValues.last_name,
                    is_admin: user.dataValues.is_admin
                }
                let token = jwt.sign(sessionData, config.secret_key, {
                    expiresIn: 1440
                });
                sessionData.token = token
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userLogin() :: with successfuly login`)
                return callback(null, func.responseGenerator(sessionData, logger.statusCodeCons.LOGIN_SUCCESS_200, logger.msgCons.LOGIN_SUCCESS, null))
            } else {    //  invalid credentials
                logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userLogin() :: with wrong password`)
                return callback(null, func.responseGenerator(null, logger.statusCodeCons.LOGIN_FAILED_200, logger.msgCons.INVALID_LOGIN_CRED, null))
            }
        } else {    //  no such user exist
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userLogin() :: with incorrect email`)
            return callback(null, func.responseGenerator(null, logger.statusCodeCons.LOGIN_FAILED_200, logger.msgCons.INVALID_LOGIN_CRED, null))
        }
        
    }).catch(err => {   //  error
        logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userLogin() :: error while checking user entry :: ${error}`)
        return callback(func.responseGenerator(null, logger.statusCodeCons.LOGIN_ERROR_500, logger.msgCons.LOGIN_ERROR, err))
    })
}

// Fetch profile
const userProfileFetch = (sessionData, callback) => {
    logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_ENTER_INTO_FUNC} :: SERVICE - userRegistration :: userProfileFetch()`)
    User.findOne({
        where: {
            user_code: sessionData.user_code
        }
    }).then(user => {
        if (user) { //  success
            delete user.dataValues.user_password
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userProfileFetch() :: with successfuly fetch user profile`)
            return callback(null, func.responseGenerator(user.dataValues, logger.statusCodeCons.USER_PROFILE_SUCCESS_200, logger.msgCons.USER_DATA_SUCCESS, null))
        } else {    //  no user found
            logger.printLog(logger.logCons.LOG_LEVEL_INFO, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userProfileFetch() :: with no user profile found`)
            return callback(null, func.responseGenerator(user.dataValues, logger.statusCodeCons.USER_PROFILE_SUCCESS_200, logger.msgCons.USER_DATA_SUCCESS_NO_DATA, null))
        }
    }).catch(error => {   //    error
        logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `${logger.logCons.LOG_EXIT_FROM_FUNC} :: SERVICE - userRegistration :: userLogin() :: error while fetching user :: ${error}`)
        return callback(func.responseGenerator(null, logger.statusCodeCons.USER_PROFILE_ERROR_500, logger.msgCons.USER_DATA_ERROR, error))
    }) 
}

module.exports = {
    userProfileFetch: userProfileFetch,
    userRegistration: userRegistration,
    userLogin: userLogin
}