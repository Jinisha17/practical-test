const express = require('express')
const bodyparser = require('body-parser')
const logger = require('./app/utils/logger')

// Database
const db = require('./app/config/database')

db.authenticate()
    .then(() => logger.printLog(logger.logCons.LOG_LEVEL_INFO, `CONNECTED`))
    .catch(err => logger.printLog(logger.logCons.LOG_LEVEL_ERROR, `connection(): ${JSON.stringify(err)}`))

//setup express
const app = express()
const PORT = 7000

//Body-parser middleware
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false}))

//initialize routes
app.use('/v1/user', require('./app/controller/userConroller'))
app.use('/v1/post', require('./app/controller/postController'))

//listen for request
app.listen(PORT,function(){
	logger.printLog(logger.logCons.LOG_LEVEL_INFO, `Listening to port ${PORT}`)
})